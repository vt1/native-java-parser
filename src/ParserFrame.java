import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by ������ on 13.07.2015.
 */
public class ParserFrame extends JFrame
{
    private Container contentPane;
    private JPanel mainPanel;
    private JTextField urlTextField;
    private JLabel urlLabel;
    private JButton submitButton;
    private URL url;
    private BufferedReader in;
    ArrayList<String> listText;
    News news;
    NewsDB newsDB;
    private static final String MDASH = "&mdash;";
    private static final String NBSP_SPACE = "&nbsp";
    private static final String TAG_TEXT = "<p>";
    private static final String TAG_TEXT_CLOSE = "</p>";
    private static final String TAG_HEADER = "<h1"; //h1,h2,h3
    private static final String TAG_HEADER_CLOSE = "</h1>"; //h1,h2,h3
    private static final String TAG_IMG = "<img";
    private static final String SRC = "src=";
    private static final String DIV_CLASS = "<div class";
    private static final String DIV_CLASS_END = "</div>";

    public ParserFrame(String title)
    {
        super(title);
        contentPane = this.getContentPane();
        newsDB = new NewsDB();
        newsDB.createDataBase();
        newsDB.getDataBase().createDataBase("NewsDataBase");
        listText = new ArrayList<String>();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        createGUI();
    }

    public void createGUI()
    {
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlowLayout());
        contentPane.add(mainPanel);
        urlTextField = new JTextField(30);
        mainPanel.add(urlTextField);
        urlLabel = new JLabel("URL:");
        mainPanel.add(urlLabel, FlowLayout.LEFT);
        submitButton = new JButton("Submit");
        mainPanel.add(submitButton);

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(!urlTextField.getText().isEmpty())
                {
                    parse(urlTextField.getText());
                }
            }
        });
    }

    public void parse(String url)
    {
        String textResult = "";
        String htmlLine = "";
        String htmlSubstring;
        String headerText = "";
        String htmlLineHelpVar;
        String img;
        news = new News();

        try
        {
            this.url = new URL(url);
            in = new BufferedReader(new InputStreamReader(this.url.openStream()));
            String inputLine;
            while((inputLine = in.readLine()) != null)
            {
                htmlLine = htmlLine.concat(inputLine);
            }
            in.close();
        }
        catch (java.io.IOException e1)
        {
            e1.printStackTrace();
        }

        htmlLineHelpVar = htmlLine;



        //while(htmlLineHelpVar.contains(DIV_CLASS))
        //{
        //    int lengthHtmlLine = htmlLineHelpVar.length();
        //    int startPosition = htmlLineHelpVar.indexOf(DIV_CLASS) + DIV_CLASS.length();
        //    int endPosition = htmlLineHelpVar.indexOf((DIV_CLASS_END), startPosition);
        //    htmlSubstring = htmlLineHelpVar.substring(startPosition, endPosition);
        //    htmlLineHelpVar = htmlLineHelpVar.substring(endPosition + DIV_CLASS.length(), lengthHtmlLine);
        //    listText.add(htmlSubstring);
        //}


        while(htmlLineHelpVar.contains(TAG_TEXT))
        {
            int lengthHtmlLine = htmlLineHelpVar.length();
            int startPosition = htmlLineHelpVar.indexOf(TAG_TEXT) + TAG_TEXT.length();
            int endPosition = htmlLineHelpVar.indexOf((TAG_TEXT_CLOSE), startPosition);
            htmlSubstring = htmlLineHelpVar.substring(startPosition, endPosition);
            htmlLineHelpVar = htmlLineHelpVar.substring(endPosition + TAG_TEXT.length(), lengthHtmlLine);
            textResult = textResult + htmlSubstring;
            listText.add(htmlSubstring);
        }

        for(int i = 0; i < listText.size(); i++)
        {
            String text = listText.get(i);
            if(!text.contains(".") || text.contains("target"))
            {
                listText.remove(i);
                i--;
            }
        }
        //for(int i = 0; i < listText.size(); i++)
        //{
        //   String text = listText.get(i);
        //    if(text.contains("target"))
        //    {
        //        listText.remove(i);
        //        i--;
        //    }
        //}


        //for(int i = 0; i < listText.size(); i++)
        //{
        //    if(listText.get(i).contains(MDASH))
        //    {
        //        String newStr = listText.get(i).replace(MDASH, "-");
        //        listText.set(i, newStr);
        //    }
        //    textResult = textResult.concat(listText.get(i));
        //}

        if(htmlLine.contains(TAG_HEADER))
        {
            int startPosition = htmlLine.indexOf(TAG_HEADER);
            startPosition = htmlLine.indexOf(">", startPosition) + 1;
            int endPosition = htmlLine.indexOf((TAG_HEADER_CLOSE), startPosition);
            headerText = htmlLine.substring(startPosition, endPosition);
        }

        htmlLineHelpVar = htmlLine;
        ArrayList<String> images = new ArrayList<String>();
        while(htmlLineHelpVar.contains(TAG_IMG))
        {
            int lengthHtmlLine = htmlLineHelpVar.length();
            int startPosition = htmlLineHelpVar.indexOf(TAG_IMG);
            startPosition = htmlLineHelpVar.indexOf(SRC, startPosition) + SRC.length() +1;
            int endPosition = htmlLineHelpVar.indexOf("\"", startPosition);
            img = htmlLineHelpVar.substring(startPosition, endPosition);
            images.add(img);
            htmlLineHelpVar = htmlLineHelpVar.substring(endPosition + TAG_IMG.length(), lengthHtmlLine);
        }
    }
}
