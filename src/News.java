import java.util.ArrayList;

/**
 * Created by Клиент on 15.07.2015.
 */
public class News
{
    private int id;
    private String url;
    private String header;
    private String text;
    private ArrayList<String> images;

    public News()
    {
        //
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public void setHeader(String header)
    {
        this.header = header;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void addImage(String image)
    {
        this.images.add(image);
    }

    public String getUrl()
    {
        return this.url;
    }

    public String getText()
    {
        return this.text;
    }

    public String getHeader()
    {
        return this.header;
    }

    public ArrayList<String> getImages()
    {
        return this.images;
    }




}
