import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Клиент on 16.07.2015.
 */
public class DataBase
{
    Connection conn = null;
    Statement stmt = null;

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/?user=root&password=rootpassword";

    static final String USER = "root";     //static
    static final String PASS = "hey123321"; //static

    public DataBase()
    {
        createСonnection();
    }

    public void createСonnection()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            JOptionPane.showMessageDialog(null, "Connection is ok");
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
        }
        catch (SQLException e)
        {
            //e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Connection is fail");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnectionExists()
    {
        if(conn != null)
            return true;
        return false;
    }

    public void createDataBase(String dbName)
    {
        String sql = "CREATE DATABASE ";
        String resultSql = sql + dbName;
        try {
            stmt.executeUpdate(resultSql);
            System.out.println("Database created successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createTables()
    {
        String sql = "CREATE TABLE news " +
                "(id INTEGER not NULL, " +
                " text VARCHAR(45), " +
                " headerText VARCHAR(10000), " +
                " PRIMARY KEY ( id ))";
        try {
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
